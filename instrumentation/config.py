from instrumentation import deck
from solid_liquid_handling_manager.solid_liquid_handling_manager_v2 import SolidLiquidHandlingManager

solid_liquid_handling_manager = SolidLiquidHandlingManager(instruments=deck.instruments)

