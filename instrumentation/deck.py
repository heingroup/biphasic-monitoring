import time

import ftdi_serial

from ftdi_serial import Serial
from supporting_libraries import RelayBoard
from vicivalve import VICI
# from ika import MagneticStirrer
from vapourtec import SF10

# print(Serial.list_device_serials())

# VICI Valve
serial = Serial(device_serial='DO027VCR', baudrate=9600)
hplc_valve = VICI(serial)

# IKA
# ika = MagneticStirrer('COM14')

# RELAY BOARD
relays = RelayBoard(Serial(device_serial='AG0JGDM1'))

#SF10
sf10 = SF10('COM8')


instruments = {}
# instruments['StirringDevice'] = ika
instruments['HPLCValve'] = hplc_valve
instruments['RelayBoard'] = relays
instruments['SF10'] = sf10

if __name__ == '__main__':
    # sf10.set_flow_rate(1)

    print(Serial.list_device_serials())
    # time.sleep(5)
    # relays.set_output(2,True)
    # hplc_valve.switch_valve(1)