from ftdi_serial import Serial
from supporting_libraries import RelayBoard

s = Serial(device_serial='AG0JGDM1')
relays = RelayBoard(s)
relays.set_output(1,True)
relays.set_output(1,False)
relays.set_output(2,True)
relays.set_output(2,False)