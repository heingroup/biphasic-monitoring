import threading
from hein_utilities.slack_integration.bots import WebClientOverride
from hein_utilities.slack_integration.parsing import ignore_bot_users
from hein_utilities.slack_integration.slack_managers import RTMControlManager
import re

bot_name='MerckBot'
channel_name='smaug'
channel_id='C035QRN0XMF'
print("load over")
bot_token, bot_name, channel_name, channel_id = bot_token, \
                                                bot_name, \
                                                channel_name, \
                                                channel_id
print(bot_token)
slack_manager = RTMControlManager(
    token=bot_token,
    channel_name=channel_name,
    channel_id=channel_id,
)


#slack_manager.post_slack_message('connected')


@slack_manager.run_on(event='message')
@ignore_bot_users
def catch_message(**payload):
    # slack_manager.post_slack_message('connected')
    message = payload['data']
    text = str(message.get('text'))
    web_client = payload['web_client']
    message_channel_id = str(message.get('channel'))
    if slack_manager.channel_id is not None:
        if slack_manager.channel_id != message_channel_id:
            return
    if re.search('test', text, re.IGNORECASE) is not None:
        with WebClientOverride(slack_manager, web_client):
            try:
                slack_manager.post_slack_message("I received your message!")
            except Exception as e:
                print("something's wrong")


threading.Thread(target=slack_manager.start_rtm_client).start()