import time

from vapourtec import SF10
from vicivalve import VICI

from supporting_libraries import RelayBoard

VAPORTECH_TO_THF = 2


def row_column_to_position_integrity(row, column):
    if column == 1:
        return 11 - row
    else:
        return 6 - row


class SolidLiquidHandlingManager:
    def __init__(self, instruments: dict):
        self.instruments = instruments

        # self.stirring_device = self.instruments['StirringDevice']
        self.hplc_valve: VICI = self.instruments['HPLCValve']
        self.relay: RelayBoard = self.instruments['RelayBoard']
        self.vapourtec: SF10 = self.instruments['SF10']
        self.is_washed = False

        self.SAMPLE_PUMP_VOL = 5  # mL
        self.PUSH_PUMP_VOL = 2.5  # mL
        self.DOSING_PUMP_VOL = 1  # mL
        self.AIR_VOLUME = 1.3  # ml air needed to transfer sample from sampleomatic head to sample loop
        self.SECOND_AIRGAP_TRANSFER = 0.085  # ml
        self.HPLC_TRIGGER_RELAY_PORT = 3
        self.SOLENOID_ORGANIC_STOP_RELAY_PORT = 1
        self.SOLENOID_TRIGGER_RELAY_PORT = 1
        self.SAMPLE_INLINE = "A"
        self.SAMPLE_BYPASS = "B"
        self.HPLC_FILL = "B"
        self.HPLC_INJECT = "A"
        self.LOOP_A_INJECT = "A"
        self.LOOP_A_LOAD = "B"
        self.LOOP_BIPHASE_INJECT = "A"
        self.LOOP_BIPHASE_LOAD = "B"
        self.VALVE_POS_1 = '1'
        self.VALVE_POS_2 = '2'

        # flow rates
        self.STOCK_DRAW_RATE = 2
        self.SAMPLE_DRAW_RATE = 1  # mL/min
        self.SOLVENT_DRAW_RATE = 5  # mL/min
        self.SOLVENT_PUSH_RATE = 1.0  # mL/min
        self.SOLVENT_DRAW_RATE_PPUMP = 3  # mL/min push pump
        self.SOLVENT_PUSH_RATE_PPUMP = 1  # mL/min  push pump
        self.SAMPLE_FLUSH_RATE = 3  # mL/min
        self.PUSH_RATE_FINAL = 0.3

        # flush volumes
        self.SAMPLE_FLUSH_VOLUME = self.SAMPLE_PUMP_VOL  # mL
        self.PUSH_FLUSH_VOLUME = self.PUSH_PUMP_VOL  # mL

        self.SYRINGE_PUMP_CALIBRATION_FACTOR = 1.024
        self.PUMP_VELOCITY_FAST = 20  # mL/min
        self.PUMP_VELOCITY_DEFAULT = 10
        self.PUMP_VELOCITY_SLOW = 2
        self.SM_MOVE_WITHDRAW = -75
        self.SM_MOVE_DISPENSE = -40

        self.SM_MOVE_WASH = -15
        self.SM_MOVE_CENTRIFUGE_DISPENSE = -15
        self.SM_MOVE_HPLC_WITHDRAW = -44
        self.SM_MOVE_TALL_WITHDRAW = -95
        # self.SM_MOVE_TALL_WITHDRAW = -112
        self.SM_MOVE_CENTRIFUGE_WITHDRAW = -56
        self.QUANTOS_STEP_INTEGRITY_VIAL = 2750
        self.VAPOURTEC_TO_ETHANOL = 1
        self.VAPOURTEC_TO_THF = 2
        self.VAPOURTEC_TO_CDI = 3
        self.VAPOURTEC_TO_DCM = 4
        self.VAPOURTEC_TO_BENZENE = 5
        self.VAPOURTEC_TO_AIR = 9
        self.VAPOURTEC_TO_FILTER_BEAKER = 10
        # ports on the Cavro pumps
        self.SAMPLE_PORT = 3
        self.SOLVENT_PORT = 2
        self.AIR_PORT = 1
        self.SM_MOVE_ADJUSTMENT = -12
        self.adjust()
        self.TRAVEL_LENGTH = 0.95
        self.SECOND_TRAVEL_LENGTH = -0.31

    def adjust(self):
        """
        When switching to a new needle
        """
        self.SM_MOVE_WITHDRAW -= self.SM_MOVE_ADJUSTMENT
        self.SM_MOVE_DISPENSE -= self.SM_MOVE_ADJUSTMENT

        self.SM_MOVE_WASH -= self.SM_MOVE_ADJUSTMENT
        self.SM_MOVE_CENTRIFUGE_DISPENSE -= self.SM_MOVE_ADJUSTMENT
        self.SM_MOVE_HPLC_WITHDRAW -= self.SM_MOVE_ADJUSTMENT
        self.SM_MOVE_TALL_WITHDRAW -= self.SM_MOVE_ADJUSTMENT
        self.SM_MOVE_CENTRIFUGE_WITHDRAW -= self.SM_MOVE_ADJUSTMENT

    def stop_stirring(self, position: int = 10, rate: int = 0):
        self.stirring_device.change_stir_speed(position, rate)

    def start_stirring(self, position: int = 10, rate: int = 500):
        self.stirring_device.change_stir_speed(position, rate)

    def start_stirring_ika(self, rate: int = 500):
        self.stirring_device.target_stir_rate = rate
        self.stirring_device.start_stirring()

    def stop_stirring_ika(self):
        self.stirring_device.stop_stirring()

    def stop_ika_until_temperature(self, temperature):
        while self.stirring_device.probe_temperature <= temperature:
            time.sleep(1)
        self.stirring_device.stop_stirring()

    def start_heating_ika(self, temperature: float = 25):
        self.stirring_device.target_temperature = temperature
        self.stirring_device.start_heating()
        import threading
        threading.Thread(target=self.stop_ika_until_temperature, args=[temperature*0.75]).start()

    def stop_heating_ika(self):
        self.stirring_device.stop_heating()

    def change_temperature(self, position: int = 10, temp: float = 25):
        self.stirring_device.change_temperature(position, temp)


    def row_column_to_position_integrity(self, row, column):
        if column == 1:
            return 11 - row
        else:
            return 6 - row

    def position_to_row_column_integrity(self, position):
        if position > 5:
            return 11 - position, 1
        else:
            return 6 - position, 2

    def position_to_row_column_ika(self, position):
        if position > 6:
            return 3, position - 6
        elif position > 3:
            return 2, position - 3
        else:
            return 1, position

#---Optional Control

    def vapourtec_flow(self, flow_rate):
        self.vapourtec.set_flow_rate(flow_rate)
        self.vapourtec.set_mode(self.vapourtec.MODE_FLOW)
        self.vapourtec.start()
        time.sleep(1)

    def stop_vapourtec_flow(self):
        self.vapourtec.stop()

    def hplc_injection(self):
        current_flow = self.vapourtec.flow_rate
        self.vapourtec_flow(0.5)
        self.hplc_valve.switch_valve(self.LOOP_BIPHASE_LOAD)

        self.relay.set_output(self.HPLC_TRIGGER_RELAY_PORT,True)
        time.sleep(3)
        self.relay.set_output(self.HPLC_TRIGGER_RELAY_PORT,False)

        time.sleep(90) # wait for HPLC to fill the loop
        self.hplc_valve.switch_valve(self.LOOP_BIPHASE_INJECT)
        self.vapourtec_flow(current_flow)




