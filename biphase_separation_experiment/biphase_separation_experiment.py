import time

from instrumentation.config import solid_liquid_handling_manager, SolidLiquidHandlingManager


class BiphaseSeparationExperiment:

    def __init__(self,
                 solid_liquid_handling: SolidLiquidHandlingManager):
        self.methods = solid_liquid_handling

    def run(self, flow_rate):
        self.methods.vapourtec_flow(flow_rate)

    def inject_organic_and_aqueous_layer(self, wait_time_between_phase_switch = 60,hplc_method_time: float = 480):
        b.inject_organic(wait_before_injection=wait_time_between_phase_switch)
        self.switch_layer(is_organic=False) # switch to aqueous
        time.sleep(hplc_method_time)
        b.inject_aqueous(wait_before_injection=wait_time_between_phase_switch)
        self.switch_layer(is_organic=True)
        time.sleep(hplc_method_time)
    def switch_layer(self, is_organic: bool):
        if is_organic:
            self.methods.relay.set_output(self.methods.SOLENOID_ORGANIC_STOP_RELAY_PORT, False)
        else:
            self.methods.relay.set_output(self.methods.SOLENOID_ORGANIC_STOP_RELAY_PORT, True)
    def inject(self, is_organic: bool,wait_before_injection: int):
        self.switch_layer(is_organic)
        time.sleep(wait_before_injection)
        self.methods.hplc_injection()

    def inject_organic(self,wait_before_injection: int):
        self.inject(is_organic=True,wait_before_injection=wait_before_injection)


    def inject_aqueous(self,wait_before_injection: int):
        self.inject(is_organic=False,wait_before_injection=wait_before_injection)



if __name__ == '__main__':
    # DEBUG MODE
    b = BiphaseSeparationExperiment(solid_liquid_handling_manager) # To create an experiment class
    b.methods.hplc_valve.switch_valve(b.methods.LOOP_BIPHASE_INJECT)
    b.run(flow_rate=0.5)
    b.switch_layer(is_organic=False)
    # b.run(flow_rate=1.0)                                  # To turn on the Vapourtec
    # time.sleep(360)
    # b.switch_layer(is_organic=True)
    # time.sleep(60)
    # b.run(flow_rate=0.5)
    # time.sleep(420)

    # for i in range(40):
        # b.inject_organic(wait_before_injection=60)
        # time.sleep(500)
        #
        # b.inject_organic_and_aqueous_layer()

    # for i in range(20):
    #     b.methods.hplc_injection()
    #     time.sleep(450)   #wait for HPLC run time
    # b.inject_aqueous(wait_before_injection=120)


